%{

	#include <stdio.h>
	#include <string.h>
	#include <stdlib.h>
	
int table[100];

%}

%token VAR VAL END HEADER BS BE IF ELSE DIS MAIN LOOP DIGIT FRAC SYM EE
%nonassoc IFX
%nonassoc ELSE
%left PL MN
%left ML DV


%%

prog: HEAD MAIN '(' ')' '%' STT END
	;

HEAD:
| HEAD HEADER
;


	
STT:
| STT AC
;


	
AC: EXPRESSION '*' {printf("exp = %d\n",$1);}
| DEC '*'
| IF EXPRESSION BS EXPRESSION '*' BE %prec IFX {if($2){printf("%d\n",$4);}}
| IF EXPRESSION BS EXPRESSION '*' BE ELSE BS EXPRESSION '*' BE {if($2){printf("%d\n",$4); printf("hi\n");}else{printf("%d\n",$9);}}
| LOOP VAR VAR BS EXPRESSION '*' BE {int i=table[$2],j=table[$3];while(i<=j){printf("%d\n",$5);i++;}}
;


DEC: TYPE ID
;

TYPE: DIGIT
| FRAC
| SYM
;

ID: VAR
| ID ',' VAR
;


EXPRESSION: VAR '~' EXPRESSION {table[$1]=$3; $$=$3;}
| DIS '(' EXPRESSION ')' {$$=$3;}
| EXPRESSION PL EXPRESSION {$$=$1+$3;}
| EXPRESSION MN EXPRESSION {$$=$1-$3;}
| EXPRESSION ML EXPRESSION {$$=$1*$3;}
| EXPRESSION DV EXPRESSION {$$=$1/$3;}
| EXPRESSION EE EXPRESSION {$$=$1==$3;}
| VAL {$$=$1;}
| VAR {$$=table[$1];}
;



%%



int yywrap(){
	return 1;
}

yyerror(char *s){
	printf("%s\n",s);
}