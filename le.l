%{

	#include <stdio.h>
	#include <string.h>
	#include <stdlib.h>
	#include "bi.tab.h"

%}

%%

"||".* {}
"digit main" {return MAIN;}
[a-z] {yylval=*yytext-'a'; return VAR;}
[0-9]+ {yylval=atoi(yytext); return VAL;}
"%/" {return BS;}
"/%" {return BE;}
"fi" {return IF;}
"esle" {return ELSE;}
"digit" {return DIGIT;}
"symbol" {return SYM;}
"~~" {return EE;}
"frac" {return FRAC;}
"ps" {return PL;}
"mn" {return MN;}
"ml" {return ML;}
"dv" {return DV;}
"display" {return DIS;}
"loop" {return LOOP;}
"%%" {return END;}
[@].+ {return HEADER;}
[ \t\n]+ {}
. {return *yytext;}


%%


int main(){
	yyin=freopen("input.c","r",stdin);
	yyparse();
	return 0;
}