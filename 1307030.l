%{
	#include <stdio.h>
	#include <string.h>
	#include <stdlib.h>
	
	char s[100];

	char key_store[50][50];
	int key_counter=0;
	int key_fetched_flag;

	char identification_store[50][50];
	int identification_counter=0;
	int identification_fetched_flag;

	char operator_store[7][7];
	int operator_counter=0;
	int operator_fetech_flag;
	
	char punctuation_store[7][7];
	int punctuation_counter=0;
	int punctuation_fetech_flag;
	
	int stm=0;
%}

%%
digit|frac|symbol|display|read|main|loop|fi|file|esle {
   int i=0;
   strcpy(s, yytext);
   
   if(key_counter>0){
		for (i=0; i<key_counter; i++){
			if(!strcmp(key_store[i], s))
				break;
		}
   }
   if(i==key_counter)
		strcpy(key_store[key_counter++], s);
}

Header1|Header2  {printf("Header Found %s.\n", yytext);}

[(].*[)] {printf("Function Found\n");}

~|groq|lsoq|~~|ps|mn|ml|dv|gr|ls  { 
	int i=0;
	strcpy(s, yytext);

	if(operator_counter>0){
		for(i=0; i<operator_counter; i++){
			if(!strcmp(operator_store[i], s))
				break;
		}
	}

	if(i==operator_counter)
		strcpy(operator_store[operator_counter++], s);
}

[*]|[%][/]|[/][%]  { 
	int i=0;
	strcpy(s, yytext);

	if(punctuation_counter>0){
		for(i=0; i<punctuation_counter; i++){
			if(!strcmp(punctuation_store[i], s))
				break;
		}
	}

	if(i==punctuation_counter)
		strcpy(punctuation_store[punctuation_counter++], s);
}

[A-Za-z]*    {
	int i=0;
	strcpy(s, yytext);

	if(identification_counter>0)
	{
	   for (i=0;i<identification_counter;i++)
			 if(!strcmp(identification_store[i], s))
			 break;
	}

	if(i==identification_counter)
		strcpy(identification_store[identification_counter++], s);
}

[%][%] {
	int i;
	printf("\nNumber of Keywords: %d\n", key_counter);
	for (i=0; i<key_counter; i++){
		printf("%s\n",key_store[i]);
	}
	printf("\n");

	printf("Number of Variables: %d\n", identification_counter);
	for (i=0;i<identification_counter;i++){
		printf("%s\n",identification_store[i]);
	}
	printf("\n");

	printf("Number of Operators: %d\n", operator_counter);
	for (i=0;i<operator_counter;i++){
		printf("%s\n",operator_store[i]);
	}
	printf("\n");
	
	printf("Number of Punctuations: %d\n", punctuation_counter);
	for (i=0;i<punctuation_counter;i++){
		printf("%s\n",punctuation_store[i]);
	}
}

["].*["]   

[ ]*  printf("");

[|][|].*  printf("Single line comment found: %s\n", yytext);

.   

\n   
%%

int yywrap(){
	return 1;
}

int main(){
	yyin=freopen("input.c", "r", stdin);
	yyout=freopen("output.txt", "w", stdout);
	yylex();
	return 0;
}
